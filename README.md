# My First Qt Project

<div align="left">

Проба пера в фреймворке Qt для C++ <br>
Вертикальной версия<br>
<img src="https://gitlab.com/KrackensCorp/my-first-qt-project/raw/master/Promo/v.jpg" alt="logo"></img><br>
Горизонтальная версия<br>
<img src="https://gitlab.com/KrackensCorp/my-first-qt-project/raw/master/Promo/h.jpg" alt="logo"></img><br>
Демонстрация<br>
<img src="https://gitlab.com/KrackensCorp/my-first-qt-project/raw/master/Promo/Animation.gif" alt="logo"></img><br>
</div>
