import QtQuick 2.12
import QtQuick.Window 2.10
import QtQuick.Controls 2.12
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Material 2.12


//Анимированная кнопка меню
Item {
    id: burger
    width: 56
    height: 56
    Material.theme: Material.Dark // Заданная Материал тема окна
    Material.accent: Material.Purple

    Rectangle {
        id: bar1
        x: 16
        y: 21
        width: 24
        height: 2
        antialiasing: true
    }

    Rectangle {
        id: bar2
        x: 16
        y: 27
        width: 24
        height: 2
        antialiasing: true
    }

    Rectangle {
        id: bar3
        x: 16
        y: 33
        width: 24
        height: 2
        antialiasing: true
    }

    property int animationDuration: 350

     state: "menu"
     states: [
       State {
         name: "menu"
       },

       State {
         name: "back"
         PropertyChanges { target: burger; rotation: 180 }
         PropertyChanges { target: bar1; rotation:  45; width: 13; x: 25;   y: 22 }
         PropertyChanges { target: bar2;                width: 17; x: 18.5; y: 26 }
         PropertyChanges { target: bar3; rotation: -45; width: 13; x: 25;   y: 30 }
       }
     ]

     transitions: [
       Transition {
         RotationAnimation { target: burger; direction: RotationAnimation.Clockwise; duration: animationDuration; easing.type: Easing.InOutQuad }
         PropertyAnimation { target: bar1;   properties: "rotation, width, x, y";    duration: animationDuration; easing.type: Easing.InOutQuad }
         PropertyAnimation { target: bar2;   properties: "rotation, width, x, y";    duration: animationDuration; easing.type: Easing.InOutQuad }
         PropertyAnimation { target: bar3;   properties: "rotation, width, x, y";    duration: animationDuration; easing.type: Easing.InOutQuad }
       }
     ]
}
