import QtQuick 2.12
import QtQuick.Window 2.10
import QtQuick.Controls 2.12
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Material 2.12
import QtGraphicalEffects 1.0

ApplicationWindow {
    id: applicationWindow
    visible: true
    width: 360
    height: 520
    opacity: 1
    title: qsTr("QMLProject")
    Material.theme: Material.Dark // Заданная Материал тема окна
    Material.accent: Material.Purple

    header: Toolsbar {
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0

        onMenuClicked: actionBarMenu.open() // Открываем меню

    // Добавляем меню
        Menu {
            id: actionBarMenu
            // С указанием расположения
            x: 500
            y: 40
            Menu {
                title: qsTr("Настройки")
                Action {
                    text: qsTr("Профиль")
                    // Обработка клика по меню
                    onTriggered: label.text = qsTr("Профиль")
                }
                Action {
                    text: qsTr("Приложение")
                    onTriggered: label.text = qsTr("Приложение")
                }
            }
            Action {
                text: qsTr("О нас...")
                onTriggered: label.text = qsTr("О нас...")
            }
        }
    }

    Label {

        id: label
        x: 167
        y: 253
        anchors.centerIn: parent
        text: qsTr("Hello World!")
        font.pixelSize: 14
    }

    ToolTip { // Всплывашка пояснение на клик
        id: toast
        delay: 50
        timeout: 2000
        x: (parent.width - width) / 2
        y: (parent.height - 100)

        background: Rectangle {
            color: "#673AB7"
            radius: 15
        }
    }

    Text {
        id: text_1
        color: "#000000"
        text: qsTr("Press Button")
        anchors.horizontalCenterOffset: 0 // Начальный текст в текстовом поле

        anchors {
            top: parent.top
            topMargin: 56
            horizontalCenter: parent.horizontalCenter
        }
    }

    Button {
        //Material.background: Material.DeepPurple // Заданный Материал фон кнопки

        anchors {
            left: parent.left
            bottom: parent.bottom
            right: parent.horizontalCenter
            margins: 8
        }
        text: qsTr("Ok") // Текст в кнопке

        // Обработка нажатия кнопки
        onClicked: {
            text_1.text = qsTr("OK Button is Pressed") // Передаем текст в text_1 при нажатии OK

            toast.text = qsTr("Hello World!") // Передаем текст в пояснение при нажатии ОК
            toast.visible = true // Передаем значение видимости в всплывашку

            console.debug("Button Ok") // Передаем текст в консоль при нажатии ОК
        }
    }

    Button {
        anchors {
            right: parent.right
            bottom: parent.bottom
            left: parent.horizontalCenter
            margins: 8
        }

        text: qsTr("Cancel") // Текст в кнопке

        // Обработка нажатия кнопки
        onClicked: {
            text_1.text = qsTr("Cancel Button is Pressed") // Текст в text_1 при нажатии Cancel
        }
    }


/*
    Button {
        text: "A button"
        ButtonStyle: ButtonStyle {
            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 25
                border.width: control.activeFocus ? 2 : 1
                border.color: "#888"
                radius: 4
                gradient: Gradient {
                    GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                    GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                }
            }
        }
    }
*/

/*
    Button {
        x: 185
        y: 472
        width: 164
        height: 40
        spacing: -2
        highlighted: false

            text: qsTr("Cancel")
            clip: false // Текст в кнопке

            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                color: button.down ? "#d6d6d6" : "#f6f6f6"
                border.color: "#26282a"
                border.width: 1
                radius: 10
            }

            // Обработка нажатия кнопки
            onClicked: {
                text_1.text = qsTr("Cancel Button is Pressed") // Текст в text_1 при нажатии Cancel
            }
        }
*/

}
