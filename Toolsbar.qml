import QtQuick 2.12
import QtQuick.Window 2.10
import QtQuick.Controls 2.12
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls.Material 2.12

// Тулбар верхний

ToolBar {
    width: 360
    height: 56
    id: root
    x: 0
    y: 0
    signal menuClicked() // Сигнал, который сообщит о клике по кнопке меню

    Rectangle {
        id: rectangle
        color: "#363636"
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0

        // Начало реализации анимированной кнопки меню
        Rectangle {
            id: rectangle2
            width: 56
            height: 56
            color: "#363636"
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0

            MenuBackButton {
                id: menuBackIcon
                x: 0
                y: 0
                width: 56
                height: 56
            }
            MouseArea {
                anchors.rightMargin: 0
                anchors.bottomMargin: 0
                anchors.leftMargin: 0
                anchors.topMargin: 0
                anchors.fill: parent
                onClicked: menuBackIcon.state = menuBackIcon.state === "menu" ? "back" : "menu"
            }
        }
        // Конец реализации анимированной кнопки меню

        Text {
            id: element1
            x: 96
            y: 20
            color: "#ffffff"
            text: qsTr("Krackens")
            anchors.right: image.left
            anchors.rightMargin: 6
            font.pixelSize: 14
        }

        Text {
            id: element2
            y: 20
            color: "#ffffff"
            text: qsTr("Corporation")
            anchors.left: image.right
            anchors.leftMargin: 6
            font.pixelSize: 14
        }

        Image {
            id: image
            x: 157
            width: 46
            height: 46
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 5
            source: "icons/logo.png"
            fillMode: Image.PreserveAspectFit
        }

        // Начало реализации меню
        Rectangle {
            color: "#363636" // Статический цвет (знаю, не хорошо)
            id: rectangle1
            x: 320
            width: 56
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0

            Image {
                id: image1
                width: 27
                height: 27
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                source: "icons/menu.png"
                fillMode: Image.PreserveAspectFit
            }

            MouseArea {
                id: menuArea
                anchors.fill: parent
                onClicked: root.menuClicked()
            }
        }
        // конец реализации кнопки меню

    }
}


/*##^##
Designer {
    D{i:5;anchors_x:48}D{i:7;anchors_x:152;anchors_y:0}
}
##^##*/
